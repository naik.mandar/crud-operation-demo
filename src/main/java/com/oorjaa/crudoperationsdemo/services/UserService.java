package com.oorjaa.crudoperationsdemo.services;

import com.oorjaa.crudoperationsdemo.entities.User;
import com.oorjaa.crudoperationsdemo.repository.MockUserRepo;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final MockUserRepo userRepository = new MockUserRepo();

    public boolean insertUser(User user) {
        return userRepository.insert(user);
    }

    public boolean updateUserInfo(User user) {
        return userRepository.insert(user);
    }

    public User getUserInfo(long id) {
        return userRepository.findById(id);
    }

    public boolean deleteUser(long id) {
        return userRepository.deleteById(id);
    }
}
