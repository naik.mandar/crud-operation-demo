package com.oorjaa.crudoperationsdemo.repository;

import com.oorjaa.crudoperationsdemo.entities.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MockUserRepo {

    List<User> users = new ArrayList<>();

    public boolean deleteById(long id) {
        for(User user: users) {
            if(user.getId() == id) {
                users.remove(user);
                return true;
            }
        }
        return false;
    }

    public User findById(long id) {
        for (User user: users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }


    public boolean insert(User user) {
        boolean listEmpty = users.isEmpty();
        if (!listEmpty) for (User currentUser : users) {
            if(currentUser.getId() == user.getId()) {
                users.set(users.indexOf(currentUser), user);
                return true;
            }
        }
        if (listEmpty) {
            users.add(user);
            return true;
        }
        return false;
    }
}
