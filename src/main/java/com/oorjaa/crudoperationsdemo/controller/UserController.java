package com.oorjaa.crudoperationsdemo.controller;

import com.oorjaa.crudoperationsdemo.entities.User;
import com.oorjaa.crudoperationsdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;


@RestController
public class UserController {

    @Autowired
    UserService userService;

    public List<User> users = new ArrayList<>();

    @PostMapping(value = "/addUser")
    public ResponseEntity<User> addUser(@RequestBody User user) {
        boolean listEmpty = users.isEmpty();
        if (!listEmpty) for (User currentUser : users) {
            if(currentUser.getId() == user.getId()) {
                return null;
            }
        } else {
            users.add(user);
        }
        return ResponseEntity.created(URI.create("/getUser"))
        .body(user);
    }

    @PutMapping("/updateUser")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        boolean listEmpty = users.isEmpty();
        if (!listEmpty) for (User currentUser : users) {
            if(currentUser.getId() == user.getId()) {
                users.set(users.indexOf(currentUser), user);
                return ResponseEntity.ok(user);
            }
        }
        return null;
    }

    @DeleteMapping("/deleteUser/{id}")
    public ResponseEntity<User> deleteUserInfo(@PathVariable("id") long id) {
        for (User user : users) {
            if(user.getId() == id) {
                users.remove(user);
                return ResponseEntity.ok(user);
            }
        }
        return null;
    }

    @GetMapping("/getUser/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") long id) {
        for (User user : users) {
            if (user.getId() == id) {
                return ResponseEntity.ok(user);
            }
        }
        return null;
    }
}
