package com.oorjaa.crudoperationsdemo;

import com.oorjaa.crudoperationsdemo.entities.User;
import com.oorjaa.crudoperationsdemo.services.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

//@SpringBootTest
public class CrudOperationsDemoTests {

    private static UserService userService;

    @BeforeAll
    public static void setup() {
        userService = new UserService();
    }

    @Test
    public void givenUser_whenInsertUser_thenReturnTrue() {
        User user = new User(1, "Mandar", "Jalgaon");

        boolean actual = userService.insertUser(user);
        boolean expected = true;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void givenExistingUser_whenUpdateUser_thenReturnTrue() {
        User updatedUserInfo = new User(1, "Mandar Naik", "Jalgaon");

        Boolean actual = userService.updateUserInfo(updatedUserInfo);
        Boolean expected = true;

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void givenUserId_whenGetUserInfo_thenReturnUserInfo() {

        //Given

        int userId = 1;
        User user = new User(1, "Mandar", "Jalgaon");
        User updatedUser = new User(1, "Mandar Naik", "Jalgaon");


        //When
        userService.insertUser(user);
        userService.updateUserInfo(updatedUser);
        user = userService.getUserInfo(userId);


//        boolean actual = userService.insertUser(user);

        String actual = user.toString();
        String expected = "User{" + "id=" + 1 + ", name='" + "Mandar Naik" + '\'' + ", address='" + "Jalgaon" + '\'' + '}';

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void givenExistingUser_whenDeleteUser_thenReturnTrue() {

        Boolean actual = userService.deleteUser(1);
        Boolean expected = true;

        Assertions.assertEquals(expected, actual);
    }
}
