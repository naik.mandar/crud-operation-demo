package com.oorjaa.crudoperationsdemo;

import com.oorjaa.crudoperationsdemo.controller.UserController;
import com.oorjaa.crudoperationsdemo.entities.User;
import org.junit.jupiter.api.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;


@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserControllerTests {
    static UserController userController;

    @BeforeAll
    public static void setUp()
    {
        userController = new UserController();
    }

    @Test
    @Order(1)
    public void testAddNewUser()
    {
        User user = new User(1, "Mandar","Jalgaon");
        ResponseEntity<User> actual = userController.addUser(user);
        System.out.println(userController.users);
        Assertions.assertEquals(HttpStatus.CREATED,actual.getStatusCode());
        Assertions.assertEquals(user.getName(),actual.getBody().getName());
        Assertions.assertEquals(user.getId(),actual.getBody().getId());
        Assertions.assertEquals(user.getAddress(),actual.getBody().getAddress());
    }

    @Test
    @Order(2)
    public void testUpdateExistingUser() {
        //given
        User updatedUser = new User(1, "Mandar Naik","Jalgaon");
        //when
        ResponseEntity<User> actual = userController.updateUser(updatedUser);
        System.out.println(userController.users);
        //then
        Assertions.assertEquals(HttpStatus.OK,actual.getStatusCode());
        Assertions.assertEquals(updatedUser.getName(),actual.getBody().getName());
        Assertions.assertEquals(updatedUser.getId(),actual.getBody().getId());
        Assertions.assertEquals(updatedUser.getAddress(),actual.getBody().getAddress());
    }

    @Test
    @Order(3)
    public void testReadExistingUser() {
        //given
        User user = new User(1, "Mandar Naik","Jalgaon");
        //when
        ResponseEntity<User> actual = userController.getUser(1);
        System.out.println(userController.users);
        //then
        Assertions.assertEquals(HttpStatus.OK,actual.getStatusCode());
        Assertions.assertEquals(user.getId(),actual.getBody().getId());
        Assertions.assertEquals(user.getName(),actual.getBody().getName());
        Assertions.assertEquals(user.getAddress(),actual.getBody().getAddress());
    }

    @Test
    @Order(4)
    public void testDeleteExistingUser() {
        //given
        User user = new User(1, "Mandar Naik","Jalgaon");
        //when
        ResponseEntity<User> actual = userController.deleteUserInfo(1);
        System.out.println(userController.users);
        //then
        Assertions.assertEquals(HttpStatus.OK,actual.getStatusCode());
        Assertions.assertEquals(user.getName(),actual.getBody().getName());
        Assertions.assertEquals(user.getId(),actual.getBody().getId());
        Assertions.assertEquals(user.getAddress(),actual.getBody().getAddress());
    }

    @Test
    public void checkRestControllerAnnotationPresence() {
        Annotation annotation = UserController.class.
                getAnnotation(RestController.class);
        if(annotation != null) {
            RestController restController = (RestController) annotation;
            Assertions.assertEquals(restController, UserController.class.getAnnotation(RestController.class));
        } else {
                Assertions.fail("Annotation Not Present");
        }
    }

    @Test
    public void testAddUserMethodWithAnnotationAndParameters() throws SecurityException, NoSuchMethodException {
        Class<UserController> resourceClass = UserController.class;
        Method method = resourceClass.getMethod("addUser", User.class);
        Annotation annotation = method.getAnnotation(PostMapping.class);
        if(annotation instanceof PostMapping postMapping) {
            Assertions.assertEquals(postMapping, resourceClass.getMethod("addUser", User.class).getAnnotation(PostMapping.class));
        } else {
            Assertions.fail("Annotation Not Present");
        }
    }

    @Test
    public void testUpdateUserInfoMethodWithAnnotationAndParameters() throws SecurityException, NoSuchMethodException {
        Class<UserController> resourceClass = UserController.class;
        Method method = resourceClass.getMethod("updateUser", User.class);
        Annotation annotation = method.getAnnotation(PutMapping.class);
        if(annotation instanceof PutMapping putMapping) {
            Assertions.assertEquals(putMapping, resourceClass.getMethod("updateUser", User.class).getAnnotation(PutMapping.class));
        } else {
            Assertions.fail("Annotation Not Present");
        }
    }

    @Test
    public void testGetUserMethodWithAnnotationAndParameters() throws SecurityException, NoSuchMethodException {
        Class<UserController> resourceClass = UserController.class;
        Method method = resourceClass.getMethod("getUser", long.class);
        Annotation annotation = method.getAnnotation(GetMapping.class);
        if(annotation instanceof GetMapping getMapping) {
            Assertions.assertEquals(getMapping, resourceClass.getMethod("getUser", long.class).getAnnotation(GetMapping.class));
        } else {
            Assertions.fail("Annotation Not Present");
        }
    }

    @Test
    public void testDeleteUserMethodWithAnnotationAndParameters() throws SecurityException, NoSuchMethodException {
        Class<UserController> resourceClass = UserController.class;
        Method method = resourceClass.getMethod("deleteUserInfo", long.class);
        Annotation annotation = method.getAnnotation(DeleteMapping.class);
        if(annotation instanceof DeleteMapping deleteMapping) {
            Assertions.assertEquals(deleteMapping, resourceClass.getMethod("deleteUserInfo", long.class).getAnnotation(DeleteMapping.class));
        } else {
            Assertions.fail("Annotation Not Present");
        }
    }
}
